const dotenv = require('dotenv');
dotenv.config();
const sgMail = require('@sendgrid/mail');

const API_KEY = process.env.SENDGRID_API_KEY;

sgMail.setApiKey(API_KEY);

const message = {
	to: 'gotth3way.apis@gmail.com',
	// to: ["desarrolloaplicacionesweb.jmlb@gmail.com", "gotth3way.contact@gmailcom"],
	// from: "gotth3way.apis@gmail.com",
	from: {
		name: 'John Moon SendGrid Tutorial ',
		email: 'desarrolloaplicacionesweb.jmlb@gmail.com',
	},
	subject: 'Hello from sendgrid',
	text: 'Hello, this is the content of the email',
	html: '<h1>Hello, this is the content in html</h1>',
};

sgMail
	.send(message)
	.then((res) => console.log(res))
	.catch((err) => console.error(err));
