# Angular & Nodejs JWT Auth

![](./screenshot.png)

## explinning app

creamos unas 'tasks' artificialmente con objetos json, y varias rutas de acceso a los datos una normal y otra
con authenticación basada en jwt, si verifyToken() es satisfactoria devolverá esas tasks privadas, sinó, una
respuesta en consecuencia. Cuando el usuario se logea, se le envía un token.

Para las pruebas de API usé postman, herramienta que genera peticiones http con verbos, como 'POST', 'GET', y
que puedes acoplarle un JWT en la cabecera, con Bearer-token.

En el front-end angular 8

## servir la app

> npm install
> cd /frontend && ng serve -o // o esto > cd /frontend && npm start // corre en el puerto 4200
> cd /backend && npm run dev // corre en el puerto 4000
