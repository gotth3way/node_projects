# MEAN CRUD with Angular and Nodejs

Elaborated example with MEAN (Mongodb, express, Angular and Nodejs)
Is a CRUD (Create, Read, Update and Delete) for employees.
I use typescript, async funtions, etc

# Tools

- ![Rest Client for VSCODE](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)
